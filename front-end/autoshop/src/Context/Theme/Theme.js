import * as React from "react";
import { ThemeProvider, createTheme } from "@mui/material/styles";
import { amber } from "@mui/material/colors";

export const ColorModeContext = React.createContext({
  toggleColorMode: () => { },
  color: "light"
});

export function Theme(props) {
  const [mode, setMode] = React.useState(
    localStorage.getItem("theme") || "light"
  );
  const colorMode = React.useMemo(
    () => ({
      toggleColorMode: () => {
        setMode((prevMode) => {
          const val = prevMode === "light" ? "dark" : "light";
          localStorage.setItem("theme", val);
          return val;
        });
      },
      color: mode
    }),
    [mode]
  );

  const theme = React.useMemo(
    () =>
      createTheme({
        palette: {
          mode,
          ...(mode === "dark"
            ? { primary: { ...amber, main: amber[300] } }
            : {}),
        },
      }),
    [mode]
  );

  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>{props.children}</ThemeProvider>
    </ColorModeContext.Provider>
  );
}
