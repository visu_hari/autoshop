echo "Getting ready"
cd front-end/autoshop/
npm i yarn
yarn
yarn build
mv build/ ../../backend/public
cd ../../backend
npm i
source env.sh
npm start