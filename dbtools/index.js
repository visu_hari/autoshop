const { MongoClient } = require("mongodb");
const catagory = require("./data/carogory.json");
const products = require("./data/products.json");
const uri = process.env.DB_URL;
if (!uri) {
  console.error("Please set mongodb uri");
  process.exit();
}
const client = new MongoClient(uri);
let count = 0;
const reTryConnect = async () => {
  try {
    count++;
    await client.connect();
  } catch (e) {
    if (count < 6) {
      console.log("RETRY", count, e);
      await new Promise((resolve) => setTimeout(resolve, 1000));
      return reTryConnect();
    }
    throw e;
  }
};
async function run() {
  try {
    await reTryConnect();
    console.log("Connected to db");
    let existingDB = await client.db().admin().listDatabases();
    if (existingDB.databases.find((i) => i.name === "App_db")) {
      throw new Error("DB exist");
    }
    const db = await client.db("App_db");

    console.log("Populating User ===>");
    const user = await db.collection("user");
    await user.insertOne({
      name: "test user1",
      phoneNumber: 1234,
      pinCode: 123456,
      address: "best place in the world",
      email: "user@user.com",
      hash: "666938d386efc4fa26c977e5caf0f86f6b13a9f029751bee8a6921bf7478327e2d66524b369507894c9c1d72571eeef748347acfc158a11b232654fecdf78b87",
      salt: "5a887be8dcac8fc6151a0beee26bc292",
      id: "a5528b5b-d0af-4f0c-a1c6-52f8263ea318",
    });
    console.log("User Created <===");

    console.log("Populating Category ===>");
    const category = await db.collection("category");
    await category.insertMany(JSON.parse(JSON.stringify(catagory)));
    console.log(" Category Created <===");

    console.log("Populating Products ===>");
    const product = await db.collection("products");
    await product.insertMany(JSON.parse(JSON.stringify(products)));
    await product.createIndex({ "$**": "text" });
    console.log("Products Created <===");
  } finally {
    // Ensures that the client will close when you finish/error
    await client.close();
    console.log("Disconnected");
  }
}
run().catch(console.dir);
