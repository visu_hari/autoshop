const user = {
  id: { type: "string" },
  name: { type: "string" },
  phoneNumber: { type: "number" },
  pinCode: { type: "number" },
  address: { type: "string" },
};
const user_subset = { ...user };
delete user_subset.id;
delete user_subset.email;
const user_create = {
  schema: {
    body: {
      type: "object",
      required: ["name", "phoneNumber"],
      properties: { ...user_subset },
    },
    response: {
      200: { ...user },
    },
  },
};

delete user_subset.name;

const user_update = {
  schema: {
    body: {
      type: "object",
      required: [],
      properties: { ...user_subset },
    },
    response: {
      201: { type: "object", properties: { ok: { type: "number" } } },
    },
  },
};
const user_response = {
  schema: {
    response: {
      200: { ...user },
    },
  },
};

module.exports = {
  user_create,
  user_response,
  user_update,
};
