const {
  order_create,
  order_response,
  order_update,
  order_response_collection,
} = require("./schema");

const { v4: uuidv4 } = require("uuid");

async function routes(fastify, options) {
  const collection = fastify.mongo.db.collection("order");
  fastify.register(async function (fastify) {
    fastify.addHook("preHandler", fastify.authenticate);
    fastify.get("/order", order_response_collection, async (request, reply) => {
      let result = await collection
        .find({ userId: request.user.uuid })
        .limit(request.query.count || 20)
        .toArray();
      return result;
    });
    fastify.get("/order/:orderId", order_response, async (request, reply) => {
      let result = await collection.findOne({
        id: request.params.orderId,
        userId: request.user.uuid,
      });
      return result;
    });

    fastify.post("/order/create", order_create, async (request, reply) => {
      let id = uuidv4();
      const result = await collection.insertOne({
        ...request.body,
        id,
        userId: request.user.uuid,
      });
      if (result === null) {
        throw new Error("Invalid value");
      }
      // let order = result.ops[0];
      reply.redirect(`/api/v1/payment/mock/pay?txid=${id}`);
      // return order;
    });

    fastify.delete("/order/:orderId", async (request, reply) => {
      const result = await collection.deleteOne({
        id: request.params.orderId,
      });
      if (result === null) {
        throw new Error("Invalid user id");
      }
      return { ok: 1 };
    });
  });
}

module.exports = routes;
