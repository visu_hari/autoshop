const { registerRouts } = require("./service");
const PORT = parseInt(process.env.PORT || "8080");
const HOST = process.env.HOST || "127.0.0.1";
const { validateToken } = require("./jwtUtils");
const path = require("path");
const fastify = require("fastify")({
  logger: true,
});
const fastifyStatic = require("fastify-static");

fastify.register(fastifyStatic, {
  root: path.join(__dirname, "../public"),
});

fastify.register(require("./db"));
fastify.register(require("fastify-jwt"), {
  secret: "supersecret",
  cookie: {
    cookieName: "x-token",
    signed: false,
  },
});
fastify.register(require("fastify-cookie"));
fastify.decorate("authenticate", validateToken);
fastify.register(require("fastify-swagger"), {
  routePrefix: "/swagger",
  exposeRoute: true,
});
fastify.get("/health", async (request, reply) => {
  return { status: "ok", ms: "user" };
});

registerRouts(fastify);

fastify.listen({ port: PORT, host: HOST }, function (err, address) {
  if (err) {
    fastify.log.error(err);
    process.exit(1);
  }
  fastify.log.info(`server listening on ${address}`);
});
