FROM node:lts-alpine as Builder
ENV NODE_ENV=production
WORKDIR /usr/src/app
COPY ["./front-end/autoshop/", "./"]
RUN npm install --production --silent && npm run build && rm -rf node_module

FROM node:lts-alpine
ENV NODE_ENV=production
ENV HOST="0.0.0.0"
WORKDIR /usr/src/app
COPY ["./backend/", "./"]
RUN npm install --production --silent
COPY --from=Builder /usr/src/app/build ./public
EXPOSE 8080
CMD ["npm", "start"]
