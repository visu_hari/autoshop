import React, { useState, useEffect, useContext } from "react";
import { styled, alpha } from "@mui/material/styles";
import InputBase from "@mui/material/InputBase";
import SearchIcon from "@mui/icons-material/Search";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import { SearchContext } from "../../Context/Search/Search";
const Search = styled("div")(({ theme }) => ({
  position: "relative",
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  "&:hover": {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginRight: theme.spacing(2),
  marginLeft: 0,
  width: "100%",
  [theme.breakpoints.up("sm")]: {
    marginLeft: theme.spacing(3),
    width: "auto",
  },
}));

const SearchIconWrapper = styled("div")(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: "100%",
  position: "absolute",
  pointerEvents: "none",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
}));
const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: "inherit",
  "& .MuiInputBase-input": {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("md")]: {
      width: "20ch",
    },
  },
}));

function CategorySearch(props) {
  const [category, setCategory] = useState("All");
  const [search, setSearchTxt] = useState(null);
  const [list, setList] = useState([]);
  const { setSearch } = useContext(SearchContext);
  const handleChange = (event) => {
    setCategory(event.target.value);
    setSearch({ categoryId: event.target.value, search });
  };
  useEffect(() => {
    fetch("/api/v1/category")
      .then((response) => {
        if (response.status !== 200) throw response;
        return response;
      })
      .then((response) => response.json())
      .then((result) => {
        setList(result);
      })
      .catch(() => {
        setList([]);
      });
  }, []);
  const handleSearch = (event) => {
    setSearchTxt(event.target.value);
    setSearch({ categoryId: category, search: event.target.value });
  };
  return (
    <>
      <Search>
        <SearchIconWrapper>
          <SearchIcon />
        </SearchIconWrapper>
        <StyledInputBase
          placeholder="Search…"
          inputProps={{ "aria-label": "search" }}
          onChange={handleSearch}
        />
      </Search>
      <FormControl sx={{ m: 1, minWidth: 80 }}>
        <InputLabel>Category</InputLabel>
        <Select
          value={category}
          onChange={handleChange}
          sx={{ minWidth: "5rem" }}
          label="Category">
          <MenuItem value={"All"}>All</MenuItem>
          {list.map((item) => (
            <MenuItem key={item.id} value={item.id}>
              {item.title}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </>
  );
}

CategorySearch.propTypes = {};

export default CategorySearch;
