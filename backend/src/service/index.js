function registerRouts(fastify) {
  fastify.register(require("./user"), { prefix: "/api/v1/" });
  fastify.register(require("./products"), { prefix: "/api/v1/" });
  fastify.register(require("./category"), { prefix: "/api/v1/" });
  fastify.register(require("./orders"), { prefix: "/api/v1/" });
  fastify.register(require("./payment"), { prefix: "/api/v1/" });
}
function getServiceDBName() {
  return `App_db`;
}
module.exports = { registerRouts, getServiceDBName };
