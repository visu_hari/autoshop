const { user_create, user_response, user_update } = require("./schema");
const { generateToken } = require("../../jwtUtils");

const { v4: uuidv4 } = require("uuid");
const auth = require("basic-auth");
const crypto = require("crypto");

async function routes(fastify, options) {
  const collection = fastify.mongo.db.collection("user");
  //public routs
  fastify.post("/user/signup", user_create, async (request, reply) => {
    let { name, pass } = auth.parse(request.headers.authorization);
    const userCheck = await collection.findOne({ email: name });
    console.log(userCheck);
    if (userCheck !== null) {
      reply.status(409);
      throw new Error("Email id already in use");
    }
    let id = uuidv4();
    let salt = crypto.randomBytes(16).toString("hex");
    let hash = crypto
      .pbkdf2Sync(name + pass + id, salt, 1000, 64, `sha512`)
      .toString(`hex`);
    const result = await collection.insertOne({
      ...request.body,
      email: name,
      hash,
      salt,
      id,
    });
    if (result === null) {
      throw new Error("Invalid value");
    }
    let user = result.ops[0];
    const token = generateToken(fastify, user);
    reply.setCookie("x-token", token, {
      path: "/",
      secure: true, // send cookie over HTTPS only
      httpOnly: true,
      sameSite: true, // alternative CSRF protection
    });
    return user;
  });

  fastify.get("/user/login", user_response, async (request, reply) => {
    let { name, pass } = auth.parse(request.headers.authorization);
    const result = await collection.findOne({ email: name });
    if (result === null) {
      reply.status(404);
      throw new Error("User not found");
    }
    if (
      result.hash !==
      crypto
        .pbkdf2Sync(name + pass + result.id, result.salt, 1000, 64, `sha512`)
        .toString(`hex`)
    ) {
      reply.status(401);
      throw new Error("Invalid id or password");
    }

    let user = result;
    const token = generateToken(fastify, user);
    reply.setCookie("x-token", token, {
      path: "/",
      secure: true, // send cookie over HTTPS only
      httpOnly: true,
      sameSite: true, // alternative CSRF protection
    });
    return user;
  });

  fastify.register(async function (fastify) {
    fastify.addHook("preHandler", fastify.authenticate);

    fastify.get("/user", user_response, async (request, reply) => {
      const result = await collection.findOne({ id: request.user.uuid });
      if (result === null) {
        throw new Error("Invalid user id");
      }
      return result;
    });

    fastify.put("/user", user_update, async (request, reply) => {
      const result = await collection.updateOne(
        { id: request.user.uuid },
        { $set: { ...request.body } }
      );
      if (result === null) {
        throw new Error("Invalid value");
      }
      reply.status(201);
      return result;
    });

    fastify.delete("/user", async (request, reply) => {
      const result = await collection.deleteOne({ id: request.user.uuid });
      if (result === null) {
        throw new Error("Invalid user id");
      }
      return { ok: 1 };
    });
    fastify.get("/user/logout", async (request, reply) => {
      reply.clearCookie("x-token");
      return { ok: 1 };
    });
  });
}

module.exports = routes;
