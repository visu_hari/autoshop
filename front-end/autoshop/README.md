# autoshop - Front-end dev setup

## Pre-req

- Nodejs V16+

- yarn

- Backend up an running in 8080 [ref](../../backend/README.md)

### Running Locally

1. Install dependencies by running `yarn i` in the current folder.

2. Start server by running `yarn start`.

Development server will proxy the backend at port `8080`.You can update the same [here](./src/setupProxy.js)

### Creating a build for local server

1. Run `yarn build `.
2. Replace the build folder in the public folder on backend by `rm -rf ../../backend/public/ && mv build/ ../../backend/public` 
