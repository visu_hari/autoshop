const order = {
  id: { type: "string" },
  address: { type: "string" },
  phoneNumber: { type: "number" },
  txnId: { type: "string" },
  cost: { type: "number" },
  products: {
    type: "array",
    items: {
      type: "object",
      properties: {
        categoryId: { type: "string" },
        productId: { type: "string" },
        name: { type: "string" },
        quantity: { type: "number" },
        cost: { type: "number" },
      },
    },
  },
};
const order_subset = { ...order };
delete order_subset.id;
delete order_subset.txnId;

const order_create = {
  schema: {
    body: {
      type: "object",
      required: ["address", "phoneNumber", "products", "cost"],
      properties: { ...order_subset },
    },
    response: {
      200: { ...order },
    },
  },
};

const order_response = {
  schema: {
    response: {
      200: { ...order },
    },
  },
};
const order_response_collection = {
  schema: {
    querystring: {
      count: { type: "number" },
    },
    response: {
      200: {
        type: "array",
        items: {
          type: "object",
          properties: { ...order },
        },
      },
    },
  },
};

module.exports = {
  order_create,
  order_response,
  order_response_collection,
};
