const category = {
  id: { type: "string" },
  title: { type: "string" },
  description: { type: "string" },
};
const category_subset = { ...category };
delete category_subset.id;

const category_create = {
  schema: {
    body: {
      type: "object",
      required: ["title"],
      properties: { ...category_subset },
    },
    response: {
      200: { ...category },
    },
  },
};

const category_update = {
  schema: {
    body: {
      type: "object",
      required: [],
      properties: { ...category_subset },
    },
    response: {
      201: { type: "object", properties: { ok: { type: "number" } } },
    },
  },
};
const category_response = {
  schema: {
    response: {
      200: { ...category },
    },
  },
};
const category_response_collection = {
  schema: {
    response: {
      200: {
        type: "array",
        items: {
          type: "object",
          properties: { ...category },
        },
      },
    },
  },
};

module.exports = {
  category_create,
  category_response,
  category_update,
  category_response_collection,
};
