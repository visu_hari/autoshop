# AutoShop

  A sample e-commerce website for automotive parts
  
## Pre-req

- Docker

- Docker-compose

## Steps to run

- Clone the repository
- locate the folder `cd autoshop`
- Start the application by running

        docker-compose up

    This will pull in required images and start the following services.
    
    | Service           | Purpose                          |
    | ----------------- | -------------------------------- |
    | autoshop-db       | Mongo db application data base.  |
    | autoshop-app      | Webserver                        |
    | autoshop-data-gen | A tool which populate demo data. |


- Once all the service are up and running you can access the application in `http://localhost:8080`

- Default user credentials.
  
    | Field    | Value         |
    | -------- | ------------- |
    | email    | user@user.com |
    | password | 123456      |

- Use `docker-compose stop` to stop and start the application.

## Documentations

### API

#### Swagger

- Swagger docs are available in the route `http://localhost:8080/swagger`

#### ThunderClient : [vs code extension](https://marketplace.visualstudio.com/items?itemName=rangav.vscode-thunder-client)

- [Collection](./Doc/thunderClient/thunder-collection_autoshop.json)
- [Environment](./Doc/thunderClient/thunder-environment_autoshop.json)

## Development Setup

- [Front-end](./front-end/autoshop/README.md)
- [Back-end](./backend/README.md)

##### [License](./LICENSE)

##### photo credits : [unsplash.com](https://unsplash.com/)