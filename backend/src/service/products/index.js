const {
  products_create,
  products_response,
  products_update,
  products_response_collection,
} = require("./schema");

const { v4: uuidv4 } = require("uuid");

async function routes(fastify, options) {
  const collection = fastify.mongo.db.collection("products");
  collection.createIndex({ "$**": "text" });

  //public routs
  fastify.get(
    "/products",
    products_response_collection,
    async (request, reply) => {
      let Query = {};
      if (request.query.search) {
        Query = { $text: { $search: request.query.search } };
      }
      let result = await collection
        .find(Query)
        .limit(request.query.count || 20)
        .toArray();
      return result;
    }
  );
  fastify.get(
    "/products/by/category/:categoryId",
    products_response_collection,
    async (request, reply) => {
      let Query = { categoryId: request.params.categoryId };
      if (request.query.search) {
        Query = { ...Query, $text: { $search: request.query.search } };
      }
      let result = await collection
        .find(Query)
        .limit(request.query.count || 20)
        .toArray();
      return result;
    }
  );
  fastify.get(
    "/products/:productId",
    products_response,
    async (request, reply) => {
      const result = await collection.findOne({
        id: request.params.productId,
      });
      if (result === null) {
        throw new Error("Invalid products id");
      }
      return result;
    }
  );
  fastify.register(async function (fastify) {
    fastify.addHook("preHandler", fastify.authenticate);
    fastify.put(
      "/products/:productId",
      products_update,
      async (request, reply) => {
        const result = await collection.updateOne(
          { id: request.params.productId },
          { $set: { ...request.body } }
        );
        if (result === null) {
          throw new Error("Invalid value");
        }
        reply.status(201);
        return result;
      }
    );
    fastify.post(
      "/products/create",
      products_create,
      async (request, reply) => {
        let id = uuidv4();
        const result = await collection.insertOne({
          ...request.body,
          id,
        });
        if (result === null) {
          throw new Error("Invalid value");
        }
        let products = result.ops[0];
        return products;
      }
    );
    fastify.delete("/products/:productId", async (request, reply) => {
      const result = await collection.deleteOne({
        id: request.params.productId,
      });
      if (result === null) {
        throw new Error("Invalid user id");
      }
      return { ok: 1 };
    });
  });
}

module.exports = routes;
