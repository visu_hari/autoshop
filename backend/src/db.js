const DB_URL = process.env.DB_URL;
if (!DB_URL) {
  console.error("DB_URL not found");
  process.exit(1);
}
const fastifyPlugin = require("fastify-plugin");
const { getServiceDBName } = require("./service");

async function dbConnector(fastify, options) {
  fastify.register(require("fastify-mongodb"), {
    forceClose: true,
    url: DB_URL,
    database: getServiceDBName(),
  });
}
module.exports = fastifyPlugin(dbConnector);
