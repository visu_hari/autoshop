const tokenAge = process.env.TOKEN_AGE || "13h";
function generateToken(fastify, user) {
  return fastify.jwt.sign({ uuid: user.id }, { expiresIn: tokenAge });
}
async function validateToken(request, reply) {
  try {
    await request.jwtVerify({ maxAge: tokenAge });
  } catch (err) {
    reply.send(err);
  }
}

module.exports = {
  generateToken,
  validateToken,
};
