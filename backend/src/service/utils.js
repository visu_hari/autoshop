const axios = require("axios");
const { getServicePath } = require("./index");
async function getUser(request) {
  let url = `${getServicePath("user")}/user`;
  console.log(url);
  let resp = await axios.get(url, {
    headers: { Authorization: request.headers.authorization },
  });
  return resp.data;
}
async function getService(request, id) {
  let url = `${getServicePath("service")}/service/${id}`;
  console.log(url);
  let resp = await axios.get(url, {
    headers: { Authorization: request.headers.authorization },
  });
  return resp.data;
}
async function deleteAllBookingForService(request, id) {
  let url = `${getServicePath("booking")}/booking/service/${id}`;
  console.log(url);
  let resp = await axios.delete(url, {
    headers: { Authorization: request.headers.authorization },
  });
  return resp.data;
}
module.exports = { getUser, getService, deleteAllBookingForService };
