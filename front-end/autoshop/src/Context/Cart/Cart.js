import React, { useState, useMemo, createContext } from "react";

export const CartContext = createContext({
  cart: null,
  setCart: () => {},
});

export function Cart(props) {
  const [cart, setCart] = useState([]);
  const ctx = useMemo(() => ({ cart, setCart }), [cart]);
  return (
    <CartContext.Provider value={ctx}>{props.children}</CartContext.Provider>
  );
}
