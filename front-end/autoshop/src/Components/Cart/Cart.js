import React, { useState, useContext, useRef } from "react";
import Badge from "@mui/material/Badge";
import ShoppingCart from "@mui/icons-material/ShoppingCart";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import Button from "@mui/material/Button";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import ListSubheader from "@mui/material/ListSubheader";
import TextField from "@mui/material/TextField";
import Box from '@mui/material/Box';

import { CartContext } from "../../Context/Cart/Cart";
import { UserContext } from "../../Context/User/User";
import { ColorModeContext } from "../../Context/Theme/Theme";
import useMediaQuery from "@mui/material/useMediaQuery";
import { useTheme } from "@mui/material/styles";
import Snackbar from "@mui/material/Snackbar";
import PaymentInputs from "../PaymentInput/PaymentInput";

export default function Cart() {
  const [open, setOpen] = useState(false);
  const { cart, setCart } = useContext(CartContext);
  const { user } = useContext(UserContext);
  const { color } = useContext(ColorModeContext);
  const [phoneNumber, setPhoneNumber] = useState(null);
  const [address, setAddress] = useState(null);
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down("sm"));
  const cardDetails = useRef({ no: "", exp: "", cvc: "" });
  const handleClickOpen = () => {
    if (!user) {
      return (window.location.hash = "/login");
    }
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const handlePay = async () => {
    let obj = {
      cost: cart?.reduce((cost, curr) => {
        return cost + curr.cost * curr.quantity;
      }, 0),
      address: address || user?.address,
      phoneNumber: phoneNumber || user?.phoneNumber,
      products: [...cart],
    };
    await fetch("/api/v1/order/create", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(obj),
      redirect: "follow",
    }).then((response) => {
      if (response.redirected) {
        window.location.href = response.url;
      }
    });
  };
  return (
    <>
      <Badge
        badgeContent={cart.length}
        color={color === "light" ? "secondary" : "primary"}
        onClick={handleClickOpen}>
        <ShoppingCart />
      </Badge>
      {cart.length ? (
        <Dialog
          open={open}
          fullScreen={fullScreen}
          fullWidth={true}
          maxWidth="md"

          onClose={handleClose}>
          <DialogTitle>Your Cart</DialogTitle>
          <DialogContent sx={{ padding: !fullScreen ? null : "0rem", }}>
            <List
              sx={{
                width: "100%",
                minWidth: "100%",
                bgcolor: "background.paper",
                padding: fullScreen ? "0rem" : "1rem",
              }}
              subheader={<ListSubheader>Products</ListSubheader>}
              disablePadding>
              {cart.map((item) => (
                <ListItem
                  key={item.productId}
                  secondaryAction={
                    <>
                      <Badge
                        badgeContent={item.quantity}
                        color="primary"></Badge>
                      <div
                        style={{
                          paddingLeft: "1rem",
                          display: "inline-block",
                        }}>
                        x Rs:{item.cost}
                      </div>
                    </>
                  }>
                  <ListItemText id={item.productId} primary={item.name} />
                </ListItem>
              ))}
              <ListItem
                secondaryAction={
                  <>
                    {`Rs:${cart?.reduce((cost, curr) => {
                      return cost + curr.cost * curr.quantity;
                    }, 0)}`}
                  </>
                }>
                <ListItemText id="total" primary={`Total bill amount :`} />
              </ListItem>
              <ListItem>
                <Box
                  component="form"
                  sx={{
                    '& .MuiTextField-root': { mb: 2, mr: 1, width: fullScreen ? "100%" : '25ch' },
                  }}
                  noValidate
                  autoComplete="off"
                >
                  <TextField
                    id="add"
                    label="Address"
                    variant="outlined"
                    value={address || user?.address}
                    onChange={(e) => setAddress(e.target.value)}
                  />
                  <TextField
                    id="phno-basic"
                    label="Phone Number"
                    variant="outlined"
                    value={phoneNumber || user?.phoneNumber}
                    onChange={(e) => setPhoneNumber(e.target.value)}
                  />
                </Box>
              </ListItem>

              <ListItem>
                <ListItemText primary={'Payment Details :'}></ListItemText>
              </ListItem>
              <ListItem>
                <Box
                  component="form"
                  noValidate
                  autoComplete="off"
                  sx={{ mb: 2 }}
                >
                  <PaymentInputs
                    onChange={(e) => {
                      if (e.target.id === "cardNumber") {
                        cardDetails.current.no = e.target.value;
                      }
                      if (e.target.id === "expiryDate") {
                        cardDetails.current.exp = e.target.value;
                      }
                      if (e.target.id === "cvc") {
                        cardDetails.current.cvc = e.target.value;
                      }
                    }}
                  />
                </Box>
              </ListItem>
            </List>
          </DialogContent>

          <DialogActions>
            <Button
              onClick={() => {
                setCart([]);
                handleClose();
              }}>
              Empty Cart
            </Button>
            <Button onClick={handleClose}>Cancel</Button>
            <Button onClick={handlePay}>Pay</Button>
          </DialogActions>
        </Dialog>
      ) : (
        <Snackbar
          open={open}
          autoHideDuration={5000}
          onClose={handleClose}
          message="Your Cart is Empty"
          anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
        />
      )
      }
    </>
  );
}
