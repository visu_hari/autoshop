import React, { useState, useEffect, useContext } from "react";

import CssBaseline from "@mui/material/CssBaseline";
import Grid from "@mui/material/Grid";
import Container from "@mui/material/Container";
import CardWrapper from "../../Components/CardWrapper/CardWrapper";
import { SearchContext } from "../../Context/Search/Search";

const arr = new Array(9).fill(0);
export default function Catalog() {
  const [cards, setCards] = useState(arr);
  const { search } = useContext(SearchContext);
  useEffect(() => {
    setCards(arr);
    let url = "/api/v1/products";
    if (search.categoryId && search.categoryId !== "All") {
      url = `/api/v1/products/by/category/${search.categoryId}`;
    }
    if (search.search) {
      url +=
        "?" +
        new URLSearchParams({
          search: search.search,
        }).toString();
    }
    fetch(url)
      .then((response) => {
        if (response.status !== 200) throw response;
        return response;
      })
      .then((response) => response.json())
      .then((result) => {
        setCards(result);
      })
      .catch(() => setCards([]));
  }, [search]);
  return (
    <>
      <CssBaseline />
      <main>
        <Container sx={{ py: 8 }} maxWidth="md">
          <Grid container spacing={4}>
            {cards.map((card, idx) => (
              <Grid item key={card?.id || idx} xs={12} sm={6} md={4}>
                <CardWrapper isLoading={!card} product={card} />
              </Grid>
            ))}
          </Grid>
        </Container>
      </main>
    </>
  );
}
