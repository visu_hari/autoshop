const products = {
  id: { type: "string" },
  categoryId: { type: "string" },
  title: { type: "string" },
  description: { type: "string" },
  details: { type: "string" },
  image: { type: "string" },
  cost: { type: "number" },
};
const products_subset = { ...products };
delete products_subset.id;

const products_create = {
  schema: {
    body: {
      type: "object",
      required: ["image", "description", "title", "categoryId", "cost"],
      properties: { ...products_subset },
    },
    response: {
      200: { ...products },
    },
  },
};

const products_update = {
  schema: {
    body: {
      type: "object",
      required: [],
      properties: { ...products_subset },
    },
    response: {
      201: { type: "object", properties: { ok: { type: "number" } } },
    },
  },
};
const products_response = {
  schema: {
    response: {
      200: { ...products },
    },
  },
};
const products_response_collection = {
  schema: {
    querystring: {
      count: { type: "number" },
      search: { type: "string" },
    },
    response: {
      200: {
        type: "array",
        items: {
          type: "object",
          properties: { ...products },
        },
      },
    },
  },
};

module.exports = {
  products_create,
  products_response,
  products_update,
  products_response_collection,
};
