import React, { useState, useContext } from "react";
import PropTypes from "prop-types";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import AccountCircle from "@mui/icons-material/AccountCircle";
import MenuItem from "@mui/material/MenuItem";
import Menu from "@mui/material/Menu";
import { ColorModeContext } from "../../Context/Theme/Theme";
import { useTheme } from "@mui/material/styles";
import Brightness4Icon from "@mui/icons-material/Brightness4";
import Brightness7Icon from "@mui/icons-material/Brightness7";
import DirectionsCarFilledIcon from "@mui/icons-material/DirectionsCarFilled";
import Cart from "../Cart/Cart";
import CategorySearch from "../CategorySearch/CategorySearch";
import Container from "@mui/material/Container";
import { UserContext } from "../../Context/User/User";
function Header(props) {
  const [anchorEl, setAnchorEl] = useState(null);
  const colorMode = useContext(ColorModeContext);
  const theme = useTheme();
  const { user, setUser } = useContext(UserContext);
  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  const handleLogout = async () => {
    setAnchorEl(null);
    try {
      await fetch("/api/v1/user/logout");
      setUser(null);
      window.location.hash = "/login";
    } catch (e) {
      console.log(e);
      setUser(null);
      window.location.hash = "/login";
    }
  };
  return (
    <>
      <AppBar position="sticky">
        <Toolbar>
          <Typography
            variant="h6"
            component="div"
            sx={{ flexGrow: 1, cursor: "pointer" }}
            onClick={() => (window.location.hash = "/")}>
            <DirectionsCarFilledIcon style={{ verticalAlign: "text-bottom" }} />{" "}
            AutoShop
          </Typography>
          <IconButton
            sx={{ ml: 1 }}
            onClick={colorMode.toggleColorMode}
            color="inherit">
            {theme.palette.mode === "dark" ? (
              <Brightness7Icon />
            ) : (
              <Brightness4Icon />
            )}
          </IconButton>

          <div>
            <Cart />
            <IconButton
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleMenu}
              color="inherit">
              <AccountCircle />
            </IconButton>

            <Menu
              id="menu-appbar"
              anchorEl={anchorEl}
              keepMounted
              open={Boolean(anchorEl)}
              onClose={handleClose}>
              {user ? (
                <MenuItem onClick={handleLogout}>Logout</MenuItem>
              ) : (
                <MenuItem onClick={() => (window.location.hash = "/login")}>
                  Login
                </MenuItem>
              )}
            </Menu>
          </div>
        </Toolbar>
      </AppBar>
      {
        props.auth ? null : (
          <AppBar position="static" color="inherit">
            <Container maxWidth="sm">
              <Toolbar>
                <CategorySearch />
              </Toolbar>
            </Container>
          </AppBar>
        )
      }
    </>
  );
}

Header.propTypes = {
  auth: PropTypes.bool,
};

export default Header;
