// eslint-disable-next-line no-restricted-globals
const ignored = self.__WB_MANIFEST;
const CACHE_NAME = "AutoShopCache"
const files = new RegExp("^/static/*")

self.addEventListener('fetch', function (event) {
    let path = new URL(event.request.url).pathname
    if (!(files.test(path) || new RegExp("^/api/v1/category").test(path))) {
        return false
    }
    event.respondWith(
        caches.match(event.request)
            .then(function (response) {
                if (response) {
                    return response;
                }
                return fetch(event.request).then(
                    function (response) {
                        if (!response || response.status !== 200 || response.type !== 'basic') {
                            return response;
                        }
                        var responseToCache = response.clone();
                        caches.open(CACHE_NAME)
                            .then(function (cache) {
                                cache.put(event.request, responseToCache);
                            });
                        return response;
                    }
                );
            })
    );
});