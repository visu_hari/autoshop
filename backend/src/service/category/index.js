const {
  category_create,
  category_response,
  category_update,
  category_response_collection,
} = require("./schema");

const { v4: uuidv4 } = require("uuid");

async function routes(fastify, options) {
  const collection = fastify.mongo.db.collection("category");
  //public routs
  fastify.get(
    "/category",
    category_response_collection,
    async (request, reply) => {
      let result = await collection.find().toArray();
      return result;
    }
  );
  fastify.get(
    "/category/:categoryId",
    category_response,
    async (request, reply) => {
      const result = await collection.findOne({
        id: request.params.categoryId,
      });
      if (result === null) {
        throw new Error("Invalid category id");
      }
      return result;
    }
  );
  fastify.register(async function (fastify) {
    fastify.addHook("preHandler", fastify.authenticate);
    fastify.put(
      "/category/:categoryId",
      category_update,
      async (request, reply) => {
        const result = await collection.updateOne(
          { id: request.params.categoryId },
          { $set: { ...request.body } }
        );
        if (result === null) {
          throw new Error("Invalid value");
        }
        reply.status(201);
        return result;
      }
    );
    fastify.post(
      "/category/create",
      category_create,
      async (request, reply) => {
        let id = uuidv4();
        const result = await collection.insertOne({
          ...request.body,
          id,
        });
        if (result === null) {
          throw new Error("Invalid value");
        }
        let category = result.ops[0];
        return category;
      }
    );
    fastify.delete("/category/:categoryId", async (request, reply) => {
      const result = await collection.deleteOne({
        id: request.params.categoryId,
      });
      if (result === null) {
        throw new Error("Invalid user id");
      }
      return { ok: 1 };
    });
  });
}

module.exports = routes;
