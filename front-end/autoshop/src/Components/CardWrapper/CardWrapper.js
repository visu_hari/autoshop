import React, { useState, useContext } from "react";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import Skeleton from "@mui/material/Skeleton";
import Chip from "@mui/material/Chip";
import Grid from "@mui/material/Grid";
import useMediaQuery from "@mui/material/useMediaQuery";
import { useTheme } from "@mui/material/styles";

import { CartContext } from "../../Context/Cart/Cart";

function CardWrapper(props) {
  let item = props.product;
  const [open, setOpen] = useState(false);
  const { cart, setCart } = useContext(CartContext);
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down("sm"));
  const AddToCart = () => {
    let currentCart = [...cart];
    let found = currentCart.find((prod) => prod.productId === item.id);
    if (found) {
      found.quantity += 1;
    } else {
      currentCart.push({
        productId: item.id,
        name: item.title,
        categoryId: item.categoryId,
        quantity: 1,
        cost: item.cost || 0,
      });
    }
    setCart(currentCart);
    handleClose();
  };

  const getCount = () => {
    let currentCart = cart;
    let found = currentCart.find((prod) => prod.productId === item.id);
    if (found) {
      return `(${found.quantity})`;
    } else {
      return "";
    }
  };
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <>
      {props.isLoading ? (
        <Card sx={{ height: "100%", display: "flex", flexDirection: "column" }}>
          <Skeleton variant="rectangular" height={118} />
          <CardContent sx={{ flexGrow: 1 }}>
            <Skeleton variant="text" />
            <Skeleton variant="text" />
            <Skeleton variant="text" />
          </CardContent>
          <CardActions>
            <Skeleton variant="rectangular" width={100} />
            <Skeleton variant="rectangular" width={100} />
          </CardActions>
        </Card>
      ) : (
        <>
          <Card
            sx={{ height: "100%", display: "flex", flexDirection: "column" }}>
            <CardMedia
              component="img"
              image={props.product.image}
              alt="your image"
              sx={{ maxHeight: "10rem" }}
            />
            <CardContent sx={{ flexGrow: 1 }}>
              <Typography gutterBottom>
                {props.product.title}
                <div style={{ float: "Right" }}>
                  <Chip label={`RS:${item.cost || 0}`} />
                </div>
              </Typography>
              <Typography variant="caption">
                {props.product.description}
              </Typography>
            </CardContent>
            <CardActions>
              <Button size="small" onClick={handleClickOpen}>
                View
              </Button>
              <Button size="small" onClick={AddToCart}>
                Add to Cart{getCount()}
              </Button>
            </CardActions>
          </Card>
          <Dialog
            open={open}
            fullWidth={true}
            fullScreen={fullScreen}
            maxWidth="md"
            onClose={handleClose}>
            <DialogTitle>
              {props.product.title}
              <div style={{ float: "Right" }}>
                <Chip label={`RS:${item.cost || 0}`} />
              </div>
            </DialogTitle>
            <DialogContent>
              <Grid container spacing={2}>
                <Grid item xs={12} md={8}>
                  <img
                    src={item.image}
                    style={{
                      width: "100%",
                      height: "60vh",
                      objectFit: "contain",
                    }}
                    alt="Product"
                  />
                </Grid>
                <Grid item xs={12} md={4}>
                  <Typography variant="h6" component="h6">
                    {props.product.description}
                  </Typography>
                  <ul>
                    {props.product.details?.split("\n").map((s, i) => (<Typography key={i + s} variant="body2" gutterBottom component="li">{s}</Typography>))}
                  </ul>
                </Grid>
              </Grid>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleClose}>Cancel</Button>
              <Button size="small" onClick={AddToCart}>
                Add to Cart {getCount()}
              </Button>
            </DialogActions>
          </Dialog>
        </>
      )}
    </>
  );
}

CardWrapper.propTypes = {};

export default CardWrapper;
