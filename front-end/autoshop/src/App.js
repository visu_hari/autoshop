import React from "react";
import Header from "./Components/Header/Header";
import "./App.css";
import { Theme } from "./Context/Theme/Theme";
import { User } from "./Context/User/User";
import { Cart } from "./Context/Cart/Cart";
import { Search } from "./Context/Search/Search";
import Login from "./Pages/Login/Login";
import Catalog from "./Pages/Catalog/Catalog";
import Order from "./Pages/Order/Order";
import { HashRouter, Routes, Route } from "react-router-dom";

function App() {
  return (
    <Theme>
      <User>
        <Cart>
          <Search>
            <HashRouter>
              <Routes>
                <Route
                  path="/login"
                  element={
                    <>
                      <Header auth={true} />
                      <Login />
                    </>
                  }></Route>
                <Route
                  path="/order"
                  element={
                    <>
                      <Header auth={true} />
                      <Order />
                    </>
                  }></Route>
                <Route
                  index
                  path="*"
                  element={
                    <>
                      <Header />
                      <Catalog />
                    </>
                  }></Route>
              </Routes>
            </HashRouter>
          </Search>
        </Cart>
      </User>
    </Theme>
  );
}

export default App;
