import React, { useState, useMemo, useEffect } from "react";

export const UserContext = React.createContext({
  user: null,
  setUser: () => {},
});

export function User(props) {
  const [user, setUser] = useState(null);
  const ctx = useMemo(() => ({ user, setUser }), [user]);
  useEffect(() => {
    fetch("/api/v1/user")
      .then((response) => {
        if (response.status !== 200) throw response;
        return response;
      })
      .then((response) => response.json())
      .then((result) => {
        setUser(result);
      })
      .catch(() => setUser(null));
  }, []);
  return (
    <UserContext.Provider value={ctx}>{props.children}</UserContext.Provider>
  );
}
