async function routes(fastify, options) {
  fastify.get("/payment/mock/pay", async (request, reply) => {
    reply.type("text/html");
    reply.send(`<!DOCTYPE html>
      <html>
      <head>
      <title>Redirecting to mock payment</title>
      </head>
      <body>
      
      <h1>Securely redirecting to payment page</h1>
      <script>setTimeout(() => {
        window.location = "/api/v1/payment/mock/bank?txid=${request.query.txid}";
      }, 3000);</script>
      
      </body>
      </html>`);
  });

  fastify.get("/payment/mock/bank", async (request, reply) => {
    reply.type("text/html");
    reply.send(`<!DOCTYPE html>
      <html>
      <head>
      <title>Redirecting to mock bank</title>
      </head>
      <body>
      
      <h1>Successful payment</h1>
      <script>setTimeout(() => {
        window.location = "/#/order?txid=${request.query.txid}";
      }, 3000);</script>
      
      </body>
      </html>`);
  });
}

module.exports = routes;
