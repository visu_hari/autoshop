import React, { useState, useMemo } from "react";

function debounce(callback, wait, immediate = false) {
  let timeout = null;

  return function () {
    const callNow = immediate && !timeout;
    const next = () => callback.apply(this, arguments);

    clearTimeout(timeout);
    timeout = setTimeout(next, wait);

    if (callNow) {
      next();
    }
  };
}

export const SearchContext = React.createContext({
  search: {},
  setSearch: () => { },
});

export function Search(props) {
  const [search, setSearch] = useState({});
  const debouncedSearch = useMemo(() => debounce(setSearch, 1000), [setSearch]);
  const ctx = useMemo(() => ({ search, setSearch: debouncedSearch }), [search, debouncedSearch]);
  return (
    <SearchContext.Provider value={ctx}>
      {props.children}
    </SearchContext.Provider>
  );
}
