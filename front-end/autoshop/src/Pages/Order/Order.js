import React, { useState, useEffect } from "react";
import Avatar from "@mui/material/Avatar";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import Badge from "@mui/material/Badge";
import CssBaseline from "@mui/material/CssBaseline";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import ListSubheader from "@mui/material/ListSubheader";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import CircularProgress from "@mui/material/CircularProgress";

export default function Order() {
  const [order, setOrder] = useState(null);
  useEffect(() => {
    let url = `/api/v1/order/${window.location.hash.split("#/order?txid=")[1]}`;
    fetch(url)
      .then((response) => {
        if (response.status !== 200) throw response;
        return response;
      })
      .then((response) => response.json())
      .then((result) => {
        setOrder(result);
      })
      .catch(() => setOrder(null));
  }, []);

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      {order ? (
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}>
          <Avatar sx={{ m: 1, bgcolor: "success" }}>
            <CheckCircleIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Order Placed Successfully
          </Typography>
          <Typography component="div" variant="caption">
            {order.id}
          </Typography>
          <List
            sx={{
              width: "100%",
              minWidth: "100%",
              bgcolor: "background.paper",
              padding: "1rem",
            }}
            subheader={<ListSubheader>Products</ListSubheader>}
            disablePadding>
            {order.products.map((item) => (
              <ListItem
                key={item.productId}
                secondaryAction={
                  <>
                    <Badge badgeContent={item.quantity} color="primary"></Badge>
                    <div
                      style={{
                        paddingLeft: "1rem",
                        display: "inline-block",
                      }}>
                      x Rs:{item.cost}
                    </div>
                  </>
                }>
                <ListItemText id={item.productId} primary={item.name} />
              </ListItem>
            ))}
            <ListItem
              secondaryAction={
                <>
                  {`Rs:${order.products?.reduce((cost, curr) => {
                    return cost + curr.cost * curr.quantity;
                  }, 0)}`}
                </>
              }>
              <ListItemText id="total" primary={`Total bill amount :`} />
            </ListItem>
            <ListItem>
              <ListItemText id="add" primary={`Delevery Address :`} />
            </ListItem>
            <ListItem>
              <Typography component="div" variant="caption">
                {order.address}
              </Typography>
            </ListItem>
            <ListItem
              secondaryAction={
                <div>
                  {order.phoneNumber}
                </div>
              }>
              <ListItemText id="phno" primary={`Phone Number :`} />
            </ListItem>
          </List>
        </Box>
      ) : (
        <CircularProgress color="inherit" />
      )}
    </Container>
  );
}
