# autoshop - Backend server dev setup

## Pre-req

- Nodejs V16+

- Docker

## Mongodb setup

    docker pull mongo

    docker run -d  --name mongo-on-docker  -p 27888:27017 -e MONGO_INITDB_ROOT_USERNAME=<username> -e MONGO_INITDB_ROOT_PASSWORD=<password> mongo

use `set DB_URL="mongodb://username:password@localhost:27888/?authSource=admin&readPreference=primary&directConnection=true&ssl=false"`

### Running Locally

1. Install dependencies by running `npm i` in the current folder.

2. Start server by running `npm start`.

### Env Variables

| Name      | Value                      | Mandatory                    |
| --------- | -------------------------- | ---------------------------- |
| DB_URL    | mongo db uri               | Yes                          |
| HOST      | Host for the server        | Optional default `127.0.0.1` |
| PORT      | Port number for the server | Optional default `8080`      |
| TOKEN_AGE | Token expire time          | Optional default `13h`       |

NOTE : By default public folder is empty. we will use proxy options to serve from frontend. To run only backend fallow the build steps [here](../front-end/autoshop/README.md#Creating-a-build-for-local-server) section - Creating a build for local server.
